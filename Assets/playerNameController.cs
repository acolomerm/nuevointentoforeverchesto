using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class playerNameController : MonoBehaviour
{

    public TMP_Text textPlayerName;
    public LoginScriptable user;

    // Start is called before the first frame update
    void Start()
    {
        textPlayerName.text = user.userName;
        if (user.userName == "")
        {
            textPlayerName.text = "Testing";
        }
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.transform.childCount; i++)
        {
            if (GameManager.Instance.playerWin == i && this.transform.GetChild(i).gameObject.active == false)
            {
                this.transform.GetChild(i).gameObject.SetActive(true);
            }
        }
    }

}
