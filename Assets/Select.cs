using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Select : MonoBehaviour
{
    public DatosPersistentes datos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < this.datos.allCards.Count; i++)
        {
            if (!this.datos.myTeam.Contains(this.datos.allCards[i])&&!this.datos.otherCards.Contains(this.datos.allCards[i]))
            {
                this.datos.otherCards.Add(this.datos.allCards[i]);
                foreach (CardsScriptable item in this.datos.otherCards)
                {
                    Debug.Log(item.name);
                }
            }

        }

        for(int j=0; j<this.transform.childCount; j++)
        {
            this.transform.GetChild(j).gameObject.GetComponent<CardDisplay>().Card = this.datos.otherCards[j];
        }
    }
}
