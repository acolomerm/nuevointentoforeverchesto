using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{

    public static GameManager Instance = null;

    // Instanciamos el tiles y el pathfinding
    public GameObject player;
    public Tilemap sceneTileMap;

    //  Lista de Cartas
    public List<CardsScriptable> allMobs = new List<CardsScriptable>();
    public List<CardsScriptable> myTeam = new List<CardsScriptable>();

    // Control sobre la batalla
    public int level = 1;
    public bool runBattle;
    public int jewel;
    public bool zonaVenta;
    public bool canBuy;
    public int reroll;
    public int timeToBattle;
    public int playerWin;
    public int oponentWin;
    public int round;

    // Lista de posiciones en la batalla
    public List<Vector3Int> enemyPosition = new List<Vector3Int>();
    public List<Vector3Int> aliesPosition = new List<Vector3Int>();

    // Listas que contienen los gameobjects que  estan en el tablero
    public List<GameObject> enemyMobs = new List<GameObject>();
    public List<GameObject> aliesMobs = new List<GameObject>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            //DontDestroyOnLoad(this);
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        //SceneManager.LoadScene("Inicio");

    }

    // Start is called before the first frame update
    private void Start()
    {
        //SceneManager.LoadScene("Inicio");
    }

}
