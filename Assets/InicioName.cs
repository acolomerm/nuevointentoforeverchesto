using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InicioName : MonoBehaviour
{
    public DatosPersistentes datos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (datos.name == "")
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = "Testing";
        }
        else
        {
            this.GetComponent<TMPro.TextMeshProUGUI>().text = datos.name;
        }
        
    }
}
