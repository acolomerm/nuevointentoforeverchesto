using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ElMeuScriptable", menuName = "Scriptables/ElMeuScriptable")]
public class ElMeuScriptable : ScriptableObject
{
    public int ValorMax;
    public int ValorActual;
}
