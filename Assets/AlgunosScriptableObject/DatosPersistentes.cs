using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level", menuName = "Scriptables/Level")]

public class DatosPersistentes : ScriptableObject
{
    public int nivell;
    public List<CardsScriptable> myTeam;
    public List<CardsScriptable> allCards;
    public List<CardsScriptable> otherCards;
    public CardsScriptable clicked;

}
