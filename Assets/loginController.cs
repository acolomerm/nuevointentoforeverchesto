using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loginController : MonoBehaviour
{

    public TMP_InputField input;

    public DatosPersistentes datos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

   public void GoToMenu()
    {

        datos.name = input.text;

        SceneManager.LoadScene("General");

    }

    public void PlayGame()
    {
        SceneManager.LoadScene("Batalla");
        GameManager.Instance.myTeam = datos.myTeam;
        GameManager.Instance.name = datos.name;
    }


}
