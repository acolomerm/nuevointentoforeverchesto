using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CardDisplay : MonoBehaviour
{

    public CardsScriptable Card;

    public TMP_Text nameText;

    public TMP_Text JewelText;
    public TMP_Text AbilitieCostText;
    public Image img;
    public TMP_Text damageText;
    public TMP_Text healthText;
    public TMP_Text rangeText;
    public TMP_Text atackSpeedText;
    public TMP_Text moveSpeedText;


    public Canvas selectTeam;
    public Canvas MyTeamCanvas;

    // Start is called before the first frame update
    void Start()
    {

        Card.setCurrentHp();

        nameText.text = "Name: " + Card.Name;
        JewelText.text = "Jewel Cost: " + Card.jewelCost;
        AbilitieCostText.text = "Abilitie Cost: " + Card.abilitieCost;
        img.sprite = Card.img[0];
        damageText.text = "Damage: " + Card.damage;
        healthText.text = "Healt: " + Card.hpMax;
        rangeText.text = "Range: " + Card.range;
        atackSpeedText.text = "Atack Speed: " + Card.atackSpeed;
        moveSpeedText.text = "Move Speed: " + Card.moveSpeed;

    }
    private void Update()
    {
        Card.setCurrentHp();

        nameText.text = "Name: " + Card.Name;
        JewelText.text = "Jewel Cost: " + Card.jewelCost;
        AbilitieCostText.text = "Abilitie Cost: " + Card.abilitieCost;
        img.sprite = Card.img[0];
        damageText.text = "Damage: " + Card.damage;
        healthText.text = "Healt: " + Card.hpMax;
        rangeText.text = "Range: " + Card.range;
        atackSpeedText.text = "Atack Speed: " + Card.atackSpeed;
        moveSpeedText.text = "Move Speed: " + Card.moveSpeed;
    }
    public void SwapCanvas()
    {
        selectTeam.gameObject.SetActive(true);
        //Activar canvas que te pasamos
        this.transform.parent.gameObject.SetActive(false);
        this.transform.parent.gameObject.GetComponent<MyTeamVisual>().datos.clicked = this.Card;

    }

    public void SwapCard()
    {
        this.transform.parent.gameObject.GetComponent<Select>().datos.myTeam.Remove(this.transform.parent.gameObject.GetComponent<Select>().datos.clicked);

        this.transform.parent.gameObject.GetComponent<Select>().datos.myTeam.Add(Card);
        this.transform.parent.gameObject.GetComponent<Select>().datos.otherCards.Remove(Card);
        MyTeamCanvas.gameObject.SetActive(true);
        //Activar canvas que te pasamos
        this.transform.parent.gameObject.SetActive(false);

    }


}
